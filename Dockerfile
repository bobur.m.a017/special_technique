FROM openjdk:17-alpine
EXPOSE 7788
ADD target/agro.jar agro.jar
ENTRYPOINT ["java","-jar","agro.jar"]

package com.example.specialTechnique;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EntityScan("com.example.specialTechnique.entity,com.example.specialTechnique.payload.CustomPage, com.example.specialTechnique.config,com.example.specialTechnique.mapper")
@EnableJpaRepositories("com.example.specialTechnique.repository")
public class SpecialTechniqueApplication {


    public static void main(String[] args) {
        SpringApplication.run(SpecialTechniqueApplication.class, args);
    }

}

package com.example.specialTechnique.company;

import java.math.BigDecimal;
import java.sql.Timestamp;

public class Company {
    private Integer id;
    private String name;
    private String email;
    private String role;
    private String api_token;
    private String status;
    private String sms_api_login;
    private String sms_api_password;
    private Integer uz_price;
    private Integer balance;
    private Integer is_vip;
    private String host;
    private String created_at;
    private String updated_at;

    @Override
    public String toString() {
        return "Users{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", role='" + role + '\'' +
                ", api_token='" + api_token + '\'' +
                ", status='" + status + '\'' +
                ", sms_api_login='" + sms_api_login + '\'' +
                ", sms_api_password='" + sms_api_password + '\'' +
                ", uz_price=" + uz_price +
                ", balance=" + balance +
                ", is_vip=" + is_vip +
                ", host=" + host +
                ", created_at=" + created_at +
                ", updated_at=" + updated_at +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getApi_token() {
        return api_token;
    }

    public void setApi_token(String api_token) {
        this.api_token = api_token;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSms_api_login() {
        return sms_api_login;
    }

    public void setSms_api_login(String sms_api_login) {
        this.sms_api_login = sms_api_login;
    }

    public String getSms_api_password() {
        return sms_api_password;
    }

    public void setSms_api_password(String sms_api_password) {
        this.sms_api_password = sms_api_password;
    }

    public Integer getUz_price() {
        return uz_price;
    }

    public void setUz_price(Integer uz_price) {
        this.uz_price = uz_price;
    }

    public Integer getBalance() {
        return balance;
    }

    public void setBalance(Integer balance) {
        this.balance = balance;
    }

    public Integer getIs_vip() {
        return is_vip;
    }

    public void setIs_vip(Integer is_vip) {
        this.is_vip = is_vip;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }
}

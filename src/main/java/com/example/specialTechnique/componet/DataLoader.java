package com.example.specialTechnique.componet;

import com.example.specialTechnique.entity.Category;
import com.example.specialTechnique.entity.ContactEntity;
import com.example.specialTechnique.entity.Role;
import com.example.specialTechnique.entity.User;
import com.example.specialTechnique.repository.CategoryRepository;
import com.example.specialTechnique.repository.ContactRepository;
import com.example.specialTechnique.repository.UserRepository;
import com.example.specialTechnique.repository.RoleRepository;
import com.example.specialTechnique.service.ContactService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class DataLoader implements CommandLineRunner {

    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final PasswordEncoder passwordEncoder;
    private final CategoryRepository categoryRepository;
    private final ContactRepository contactRepository;
    private final ContactService contactService;

    @Value("${spring.sql.init.mode}")
    private String mode;




    @Override
    public void run(String... args) throws Exception {

//        if (mode.equals("always")) {
//            Role admin = new Role("ADMIN");
//            Role saveRole = roleRepository.save(admin);
//
//            User user = new User(
//                    "admin",
//                    passwordEncoder.encode("admin123"),
//                    true,
//                    saveRole);
//            userRepository.save(user);
//
//            Category category = new Category(
//                    "Yengil yechim",
//                    1
//            );
//            categoryRepository.save(category);
//
//        }
    }
}

package com.example.specialTechnique.config;

//import com.example.specialTechnique.user.User;
import com.example.specialTechnique.entity.User;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.crypto.SecretKey;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Collection;
import java.util.Date;


@Component
public class ApplicationJwtTokenFilter extends OncePerRequestFilter {
    private String secretKey = "HJAtS9ALVpOPTCXw5W0Ifx2sHcBwmTKNKaqTgwD4";
    SecretKey key = Keys.hmacShaKeyFor(secretKey.getBytes(StandardCharsets.UTF_8));

    private final ByUserDetails byUserDetailes;

    public ApplicationJwtTokenFilter(ByUserDetails byUserDetails) {
        this.byUserDetailes = byUserDetails;
    }

    @Override
    protected void doFilterInternal(
            HttpServletRequest httpServletRequest,
            HttpServletResponse httpServletResponse,
            FilterChain filterChain
    ) throws ServletException, IOException {

        String bearerToken = httpServletRequest.getHeader("Authorization");

        if (bearerToken == null || !bearerToken.startsWith("Bearer ")) {
            filterChain.doFilter(httpServletRequest, httpServletResponse);
            return;
        }
        String token = bearerToken.replace("Bearer ", "");

        if (!isValidToken(token)) {
            filterChain.doFilter(httpServletRequest, httpServletResponse);
            return;
        }

        String phoneNumber = getClaims(token).getBody().getSubject();
        UserDetails userDetails = byUserDetailes.loadUserByUsername(phoneNumber);
        User user = (User) userDetails;
        Collection<? extends GrantedAuthority> userRoles = userDetails.getAuthorities();
        UsernamePasswordAuthenticationToken authenticationToken =
                new UsernamePasswordAuthenticationToken(
                        phoneNumber,
                        null,
                        userRoles
                );

        SecurityContextHolder.getContext().setAuthentication(authenticationToken);
        filterChain.doFilter(httpServletRequest, httpServletResponse);
    }

    private Jws<Claims> getClaims(String token) {

        Jws<Claims> jws = null;
        try {
            jws = Jwts.parserBuilder().setSigningKey(key).build().parseClaimsJws(token);
        } catch (JwtException ignored) {

        }
        return jws;

    }

    private boolean isValidToken(String token) {
        Claims claims = getClaims(token).getBody();
        Date expiryDate = claims.getExpiration();
        return expiryDate.getTime() > new Date().getTime();
    }
}

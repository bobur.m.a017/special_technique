package com.example.specialTechnique.config;

import com.example.specialTechnique.entity.User;
import com.example.specialTechnique.repository.UserRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ByUserDetails implements UserDetailsService {

    private final UserRepository userRepository;

    public ByUserDetails(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<User> byUsername = userRepository.findByUserName(username);
        return byUsername.orElse(null);
    }
}

package com.example.specialTechnique.controller;

import com.example.specialTechnique.payload.AttachmentDto;
import com.example.specialTechnique.service.AttachmentService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@RestController
@RequestMapping("/agromash/api/attachment")
@RequiredArgsConstructor
public class AttachmentController {
    private final AttachmentService attachmentService;

    @PostMapping(value = "/uploadSystem/{productId}/{isMain}")
    public HttpEntity<?> uploadFileSystem(MultipartHttpServletRequest request, @PathVariable Long productId, @PathVariable boolean isMain) throws IOException {
        return attachmentService.uploadSystem(request, productId, isMain);
    }

    @GetMapping("/downloadSytem/{id}")
    public void getOne(@PathVariable Integer id, HttpServletResponse response) throws IOException {
         attachmentService.getOne(id, response);
    }

    @PutMapping("/edit/{imageId}/{productId}")
    public HttpEntity<?> editPhotoIsMain(@PathVariable Integer imageId, @PathVariable Long productId) {
        return attachmentService.editPhotoIsMain(imageId, productId);
    }

    @GetMapping("/info/{id}")
    public HttpEntity<AttachmentDto> getOneInfo(@PathVariable Integer id) {
        return attachmentService.getOneInfo(id);
    }

    @DeleteMapping("/delete/{id}")
    public HttpEntity<?> delete(@PathVariable Integer id) {
        return attachmentService.delete(id);
    }
}

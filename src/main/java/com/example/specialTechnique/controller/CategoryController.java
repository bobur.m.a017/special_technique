package com.example.specialTechnique.controller;

import com.example.specialTechnique.entity.Category;
import com.example.specialTechnique.payload.CategoryByIndexDto;
import com.example.specialTechnique.payload.CategoryDto;
import com.example.specialTechnique.payload.CategoryListForIndexDto;
import com.example.specialTechnique.payload.CategoryResDto;
import com.example.specialTechnique.service.CategoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/agromash/api/category")
@RequiredArgsConstructor
public class CategoryController {
    private final CategoryService categoryService;

    @PostMapping("/add")
    public HttpEntity<?> add(@RequestBody @Valid CategoryDto categoryDto) {
        return categoryService.add(categoryDto);
    }

    @DeleteMapping("/delete/{id}")
    public HttpEntity<?> delete(@PathVariable Integer id) {
        return categoryService.delete(id);
    }

    @PutMapping("/edit/{id}")
    public HttpEntity<?> edit(@RequestBody CategoryDto categoryDto, @PathVariable Integer id) {
        return categoryService.edit(categoryDto, id);
    }

    @PutMapping("/edit/index")
    public HttpEntity<?> editIndex(@RequestBody List<CategoryByIndexDto> categorysDto) {
        return categoryService.editIndex(categorysDto);
    }

    @GetMapping("/get-one/{id}")
    public HttpEntity<?> getOne(@PathVariable Integer id) {
        return categoryService.getOne(id);
    }

    @GetMapping("/get-all")
    public HttpEntity<?> getAll() {
        return categoryService.getAll();
    }


}

package com.example.specialTechnique.controller;

import com.example.specialTechnique.contact.ContactDTO;
import com.example.specialTechnique.entity.ContactEntity;
import com.example.specialTechnique.service.ContactService;
import com.example.specialTechnique.message.StateMessage;
import lombok.RequiredArgsConstructor;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;


@RestController
@RequestMapping("/agromash/api/contact")
@RequiredArgsConstructor
public class ContactController {

    private  final ContactService contactService;


    @PostMapping(value = "/add")
    public HttpEntity<?> add(@RequestBody ContactDTO dto) throws IOException {
        dto.setGroupId("client");
        dto.setEmail("test@test.uz");
        StateMessage stateMessage = contactService.add(dto);

        return ResponseEntity.ok(stateMessage);
    }

    @PostMapping(value = "/addAll")
    public HttpEntity<?> addAll(MultipartHttpServletRequest request) throws IOException, InvalidFormatException {

        MultipartFile file = request.getFile("file");
        StateMessage stateMessage = contactService.addAll(file);

        return ResponseEntity.ok(stateMessage);
    }


    @DeleteMapping(value = "/delete/{id}")
    public HttpEntity<?> delete(@PathVariable Integer id) throws IOException {
        StateMessage stateMessage = contactService.delete(id);
        return ResponseEntity.ok(stateMessage);
    }

    @GetMapping(value = "/{id}")
    public HttpEntity<?> get(@PathVariable Integer id) throws IOException {
        ContactEntity contactEntity = contactService.get(id);
        return ResponseEntity.ok(contactEntity);
    }

    @GetMapping
    public HttpEntity<?> get() throws IOException {
        List<ContactEntity> list = contactService.getAll();
        return ResponseEntity.ok(list);
    }

    @GetMapping("/template")
    public HttpEntity<?> getTemplate() throws IOException {
//        File file = new File("/root/files/contacts.xlsx");

        FileInputStream fileInputStream = new FileInputStream("/C:/Users/User/Desktop/file/contacts.xlsx");

        File file = new File("/C:/Users/User/Desktop/file/contacts.xlsx");
        File parentFile = file.getParentFile();


        return ResponseEntity.ok(parentFile);
    }


}

package com.example.specialTechnique.controller;

import com.example.specialTechnique.payload.CustomPage;
import com.example.specialTechnique.payload.ProductDto;
import com.example.specialTechnique.payload.ProductForCategoryViewDto;
import com.example.specialTechnique.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpEntity;
import org.springframework.web.bind.annotation.*;

import static com.example.specialTechnique.utils.AppConstant.DEFAULT_PAGE_NUMBER;
import static com.example.specialTechnique.utils.AppConstant.DEFAULT_PAGE_SIZE;

@RestController
@RequestMapping("/agromash/api/product")
@RequiredArgsConstructor
public class ProductController {
    private final ProductService productService;

    @PostMapping("/add")
    public HttpEntity<?> add(@RequestBody ProductDto productDto) {
        return productService.add(productDto);
    }

    @PutMapping("/edit/{id}")
    public HttpEntity<?> edit(@PathVariable Long id, @RequestBody ProductDto productDto) {
        return productService.edit(id, productDto);
    }

    @DeleteMapping("/delete/{id}")
    public HttpEntity<?> delete(@PathVariable Long id) {
        return productService.delete(id);
    }

    //PRODUCTNI USTIGA BOSGANDA TO'LIQ MALUMOTLARI BILAN QAYTARADIGAN YO'L
    @GetMapping("/get-one/{productId}")
    public HttpEntity<?> getOne(@PathVariable Long productId) {
        return productService.getOne(productId);
    }

    //CATEGORIYA ID ORQALI SHU CATEGORIYANING BARCHA PRODUCTLARINI PAGEGA O'RAB QAYTARADI
    @GetMapping("get-page-product/{categoryId}")
    public HttpEntity<CustomPage<ProductForCategoryViewDto>> getPageProductById(
            @RequestParam(defaultValue = DEFAULT_PAGE_NUMBER) int page,
            @RequestParam(defaultValue = DEFAULT_PAGE_SIZE) int size,
            @PathVariable Integer categoryId) {
        return productService.pageProduct(page, size, categoryId);
    }

    //ILK KO'RININISH BARCHA KATEGORIYALARNI ICHIDAGI N TA PRODUCTTI BILAN QAYTARDI
    @GetMapping("/get-view")
    public HttpEntity<?> getView() {
        return productService.getView();
    }


    //SEARCH
    @GetMapping("/search")
    public HttpEntity<?> search(@Param("search") String search) {


        return  productService.search(search);
    }
}

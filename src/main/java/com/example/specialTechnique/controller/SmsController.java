package com.example.specialTechnique.controller;

import com.example.specialTechnique.company.Company;
import com.example.specialTechnique.service.ContactService;
import com.example.specialTechnique.message.StateMessage;
import com.example.specialTechnique.service.SmsService;
import com.example.specialTechnique.sms.SmsDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;


@RestController
@RequestMapping("/agromash/api/sms")
public class SmsController {

    @Autowired
    ContactService contactService;
    @Autowired
    SmsService smsService;

    @PostMapping(value = "/send")
    public HttpEntity<?> sendSms(@RequestBody SmsDto dto) throws IOException {

        StateMessage stateMessage = smsService.sendSms(dto);
        return ResponseEntity.ok(stateMessage);
    }

    @GetMapping(value = "/balance")
    public HttpEntity<?> balance() throws IOException {
        Company company = smsService.getBalance();
        return ResponseEntity.ok(company);
    }
}

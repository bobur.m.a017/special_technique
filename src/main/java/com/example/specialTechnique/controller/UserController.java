package com.example.specialTechnique.controller;

import com.example.specialTechnique.config.ApplicationUsernamePasswordAuthenticationFilter;
import com.example.specialTechnique.user.ResponseUsers;
import com.example.specialTechnique.user.UsersService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/agromash/api/user")
public class UserController {


    private final UsersService usersService;

    public UserController(UsersService usersService, ApplicationUsernamePasswordAuthenticationFilter authenticationFilter) {
        this.usersService = usersService;
    }

    @PostMapping("/signIn")
    public ResponseEntity<?> signIn(HttpServletRequest request, HttpServletResponse response) {

        try {
            ResponseUsers responseUser = usersService.signIn(request, response);
            return ResponseEntity.status(responseUser.getSuccess() ? 200 : 500).body(responseUser);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }

}

package com.example.specialTechnique.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class Attachment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(nullable = false)
    private String path;  // papkani ichidan topish un


    //CLIENTGA KO'RINADIGAN NOM
    private String fileOriginalName;

    //PAPKADA KO'RINADIGAN NOMI UNIQUE BO'LADI
    private String name;//izlaganda shu nom bilan izlaniladi
    //    -----------------------------------------------
    @JoinColumn(insertable = false, updatable = false)
    @ManyToOne
    private Product product;

    @Column(name = "product_id", nullable = false)
    private Long productId;
    //    ---------------------
    private long size;

    private String contentType;
    private String src;

    private boolean isprime;

    public Attachment(String name, long size, String contentType, boolean isprime) {
        this.name = name;
        this.size = size;
        this.contentType = contentType;
        this.isprime = isprime;
    }


}

package com.example.specialTechnique.entity;

import com.example.specialTechnique.entity.template.AbsIntEntity;
import com.example.specialTechnique.entity.template.AbsLongEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Category {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotBlank
    private String name;

    private int index;

    public Category(@NotBlank String name, int index) {
        this.name = name;
        this.index = index;
    }

    public Category(@NotBlank String name) {
        this.name = name;
    }
}

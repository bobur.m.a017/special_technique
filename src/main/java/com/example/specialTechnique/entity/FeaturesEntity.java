package com.example.specialTechnique.entity;

import com.example.specialTechnique.entity.template.AbsIntEntity;
import com.example.specialTechnique.payload.FeaturesDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class FeaturesEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String name;
    private String value;

    @JoinColumn(insertable = false, updatable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private Product product;

    @Column(name = "product_id", nullable = false)
    private Long productId;

    public FeaturesEntity(FeaturesDto featuresDto) {
        this.name = featuresDto.getName();
        this.value = featuresDto.getValue();
    }

    public FeaturesEntity(String name, String value, Long productId) {
        this.name = name;
        this.value = value;
        this.productId = productId;
    }
}

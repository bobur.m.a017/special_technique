package com.example.specialTechnique.entity;

import com.example.specialTechnique.entity.template.AbsLongEntity;
import com.example.specialTechnique.payload.FeaturesDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Fetch;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class Product extends AbsLongEntity {

    private String name;

    private String brand;

    @Column(columnDefinition = "text")
    private String description;

    private Double price;

    //BAHO O'ZGARGANDA ESKI SUMMANI O'ZIDA SAQLAYDI
    private Double oldPrice;

    @OneToMany(mappedBy = "product")
    private List<FeaturesEntity> featuresDtoList;

    private boolean status = true;

    //product to'g'risida youtubeda video bo'sa linkini kiritishi uchun
    private String link;

    //-----------------------------------------------------
//    CATEGORIYA IDSINI BERVORGANDA CATEGORIYA JPA FUNKSIYASI ORQALI PRODUCTGA BIRIKTIRILADI
    @JoinColumn(insertable = false, updatable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private Category category;

    @Column(name = "category_id", nullable = false)
    private Integer categoryId;
//-----------------------------------------------------


}

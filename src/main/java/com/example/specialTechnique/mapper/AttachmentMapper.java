package com.example.specialTechnique.mapper;

import com.example.specialTechnique.entity.Attachment;
import com.example.specialTechnique.payload.AttachmentDto;
import com.example.specialTechnique.service.AttachmentService;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface AttachmentMapper {

    AttachmentDto attachmentToDto(Attachment attachment);

}

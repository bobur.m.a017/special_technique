package com.example.specialTechnique.mapper;

import com.example.specialTechnique.entity.Category;
import com.example.specialTechnique.payload.CategoryDto;
import com.example.specialTechnique.payload.CategoryResDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

@Mapper(componentModel = "spring")
public interface CategoryMapper {

    @Mapping(target = "id", ignore = true)
    void updateWithoutId(@MappingTarget Category category, CategoryDto categoryDto);
}

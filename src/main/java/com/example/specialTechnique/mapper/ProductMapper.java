package com.example.specialTechnique.mapper;

import com.example.specialTechnique.entity.Product;
import com.example.specialTechnique.payload.GetOneProductDto;
import com.example.specialTechnique.payload.ProductDto;
import com.example.specialTechnique.payload.ProductForCategoryViewDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

@Mapper(componentModel = "spring")
public interface ProductMapper {

    Product dtoToProduct(ProductDto productDto);

    GetOneProductDto productToDto(Product product);

    ProductForCategoryViewDto productToViewDto(Product product);

    //DTODAGI MA'LUMOTLARNI PRODUCTGA SET QILIBERADI
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "featuresDtoList", ignore = true)
    void updateProductToDto(ProductDto productDto, @MappingTarget Product product);
}

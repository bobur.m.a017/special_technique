package com.example.specialTechnique.message;

import org.springframework.stereotype.Component;

@Component
public class StateMessage {

    private String message;
    private boolean state;

    public StateMessage() {
    }

    public StateMessage(String message, boolean state) {
        this.message = message;
        this.state = state;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isState() {
        return state;
    }

    public void setState(boolean state) {
        this.state = state;
    }
}

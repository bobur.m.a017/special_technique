package com.example.specialTechnique.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class AttachmentDto {
    private Integer id;
    private String name;
    private String fileOriginalName;
    private long size;
    private String contentType;
}

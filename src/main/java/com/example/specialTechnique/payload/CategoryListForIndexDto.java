package com.example.specialTechnique.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class CategoryListForIndexDto {

    private List<CategoryByIndexDto> categoryList;

}

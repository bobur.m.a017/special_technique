package com.example.specialTechnique.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GetOneProductDto {
    private Long id;
    @NotBlank
    private String name;

    private String brand;

    private String description;

    @NotNull
    private Double price;

    //BAHO O'ZGARGANDA ESKI SUMMANI O'ZIDA SAQLAYDI
    private Double oldPrice;

    private boolean status = true;

    //product to'g'risida youtubeda video bo'sa linkini kiritishi uchun
    private String link;

    private Integer categoryId;

    private List<FeaturesDto> featuresDtoList;

    private List<AttachmentDto> listBytes;

}

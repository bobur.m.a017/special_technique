package com.example.specialTechnique.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LoginDto {
    @NotNull(message = "Telefon raqamini kiritmadingiz!")
    private String username;
    @NotBlank(message = "Parol bo`sh bo`lmasligi kerak!")
    private String password;


}

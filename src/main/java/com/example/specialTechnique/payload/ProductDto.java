package com.example.specialTechnique.payload;

import com.example.specialTechnique.entity.Attachment;
import com.example.specialTechnique.entity.Category;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class ProductDto {


    @NotBlank
    private String name;

    private String brand;

    private String description;

    @NotNull
    private Double price;

    //BAHO O'ZGARGANDA ESKI SUMMANI O'ZIDA SAQLAYDI
    private Double oldPrice;

    private boolean status = true;

    //product to'g'risida youtubeda video bo'sa linkini kiritishi uchun
    private String link;

    private Integer categoryId;

    private List<FeaturesDto> featuresDtoList;
}

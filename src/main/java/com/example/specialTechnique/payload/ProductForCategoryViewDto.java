package com.example.specialTechnique.payload;

import com.example.specialTechnique.entity.FeaturesEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductForCategoryViewDto {

    private Long id;

    @NotBlank
    private String name;

    @NotNull
    private Double price;

    //BAHO O'ZGARGANDA ESKI SUMMANI O'ZIDA SAQLAYDI
    private Double oldPrice;

    private String brand;
//    private List<FeaturesEntity> featuresDtoList;

    private byte[] listBytes;

}

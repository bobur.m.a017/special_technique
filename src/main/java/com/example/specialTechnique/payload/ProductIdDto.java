package com.example.specialTechnique.payload;

import lombok.Data;

@Data
public class ProductIdDto {

    private Long productId;

}

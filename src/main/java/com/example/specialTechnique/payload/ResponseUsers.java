package com.example.specialTechnique.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class ResponseUsers {

    private UUID id;
    private String role;
    private String token;
    private Boolean success;
    private String password;
    private String name;

    public ResponseUsers(UUID id, String token, Boolean success, String password, String name) {
        this.id = id;
        this.token = token;
        this.success = success;
        this.password = password;
        this.name = name;
    }

    public ResponseUsers(UUID id, String role, String token, Boolean success, String password) {
        this.id = id;
        this.role = role;
        this.token = token;
        this.success = success;
        this.password = password;
    }

}

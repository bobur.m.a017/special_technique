package com.example.specialTechnique.payload;

public class SignInUsers {


    private String login;
    private String password;

    public SignInUsers(String login, String password) {
        this.login = login;
        this.password = password;
    }

    public SignInUsers() {
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


}

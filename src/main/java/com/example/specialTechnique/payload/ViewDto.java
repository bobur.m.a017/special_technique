package com.example.specialTechnique.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class ViewDto {
    private Integer categoryId;
    private String categoryName;
    private Integer categoryIndex;
    private List<ProductForCategoryViewDto> limitProducts;
}

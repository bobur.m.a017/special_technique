package com.example.specialTechnique.repository;

import com.example.specialTechnique.entity.Attachment;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface AttachmentRepository extends JpaRepository<Attachment, Integer> {
    List<Attachment> findAllByProductId(Long productId);

    List<Attachment> findAllByProductIdOrderByIsprimeDesc(Long productId);


    Optional<Attachment> findAttachmentById(Integer id);

    Optional<Attachment> findAttachmentByIdAndProductId(Integer id, Long productId);

    Optional<Attachment> findFirstByProductId(Long productId);

    Optional<Attachment> findFirstByProductIdAndIsprimeIsTrue(Long productId);

    List<Attachment> findAllByIsprimeIsTrueAndProductId(Long productId);

}

package com.example.specialTechnique.repository;

import com.example.specialTechnique.entity.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import javax.validation.constraints.NotBlank;
import java.util.List;
import java.util.Optional;

public interface CategoryRepository extends JpaRepository<Category, Integer> {

    Optional<Category> findByName(String name);

    @Query(value = "select * from category order by index asc", nativeQuery = true)
    List<Category> findAllOrderByIndex();

    boolean existsById(Integer id);

}

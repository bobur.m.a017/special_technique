package com.example.specialTechnique.repository;

import com.example.specialTechnique.entity.ContactEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.swing.text.html.Option;
import java.util.Optional;

@Repository
public interface ContactRepository extends JpaRepository<ContactEntity,Integer> {

    Optional<ContactEntity> findByIdContact(Integer id);
    Optional<ContactEntity> findByPhoneNumber(String number);
}

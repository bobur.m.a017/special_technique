package com.example.specialTechnique.repository;

import com.example.specialTechnique.entity.Category;
import com.example.specialTechnique.entity.FeaturesEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface FeaturesEntityRepository extends JpaRepository<FeaturesEntity, Integer> {


}

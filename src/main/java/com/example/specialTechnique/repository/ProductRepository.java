package com.example.specialTechnique.repository;

import com.example.specialTechnique.entity.Product;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

public interface ProductRepository extends JpaRepository<Product, Long> {


    boolean existsByCategoryId(Integer category_id);

    Page<Product> findAllByCategoryIdOrderByUpdatedAtDesc(Integer categoryId, Pageable pageable);


    @Query(value = "select * from product as p where p.category_id = :categoryId" +
            " ORDER BY updated_at DESC limit :limit", nativeQuery = true)
    List<Product> findByCategoryIdByLimit(@Param("categoryId") Integer categoryId,
                                          @Param("limit") int limit);
}

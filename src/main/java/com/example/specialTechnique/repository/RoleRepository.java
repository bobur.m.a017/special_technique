package com.example.specialTechnique.repository;

import com.example.specialTechnique.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Integer> {

}

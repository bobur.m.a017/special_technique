package com.example.specialTechnique.response;

public class AddContactResponse {
    private String status;
    private DataAddContact data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public DataAddContact getData() {
        return data;
    }

    public void setData(DataAddContact data) {
        this.data = data;
    }
}

package com.example.specialTechnique.response;

public class DataAddContact {
    private Integer contact_id;

    @Override
    public String toString() {
        return "Data{" +
                "token='" + contact_id + '\'' +
                '}';
    }

    public Integer getContact_id() {
        return contact_id;
    }

    public void setContact_id(Integer contact_id) {
        this.contact_id = contact_id;
    }
}

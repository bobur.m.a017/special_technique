package com.example.specialTechnique.service;

import com.example.specialTechnique.entity.Attachment;
import com.example.specialTechnique.mapper.AttachmentMapper;
import com.example.specialTechnique.message.StateMessage;
import com.example.specialTechnique.payload.AttachmentDto;
import com.example.specialTechnique.repository.AttachmentRepository;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.UUID;


@Getter
@Service
@RequiredArgsConstructor
public class AttachmentService {

    private final AttachmentRepository attachmentRepository;
    private final AttachmentMapper attachmentMapper;

    //FAYLLAR TURADIGAN PAPKANING NOMI
    public static final String uploadDirectorys = "/root/agro/files";

    public HttpEntity<?> uploadSystem(MultipartHttpServletRequest request, Long productId, boolean isMain) throws IOException {

        Iterator<String> fileNames = request.getFileNames();
        while (fileNames.hasNext()) {
            MultipartFile file = request.getFile(fileNames.next());
            if (file != null) {
                String originalFilename = file.getOriginalFilename();
                Attachment attachment = new Attachment();
                //RASM QAYSI PRODUCTNIKILIGINI BIRIKTIRYAPMIZ
                attachment.setProductId(productId);
                attachment.setFileOriginalName(originalFilename);
                attachment.setSize(file.getSize());
                attachment.setContentType(file.getContentType());
                //FILENING CONTENTINI OLISH UCHUN KERAK
                String[] split = originalFilename.split("\\.");

                //rasm nameni unique qilish uchun kerak
                String name = UUID.randomUUID().toString() + "." + split[split.length - 1];
                attachment.setName(name);
                //papka saqlanadigan yo'l
                Path path = Paths.get(uploadDirectorys + "/" + name);
                attachment.setPath(path.toString());
                attachment.setIsprime(isMain);
                Attachment saveAttachment = attachmentRepository.save(attachment);

                if(isMain)
                editIsMain(saveAttachment.getId(),productId);

                Files.copy(file.getInputStream(), path);

                return ResponseEntity.ok(new StateMessage("fayl systema(papka)ga saqlandi",true));

            }
        }
        return ResponseEntity.status(500).body("Fayl sistemaga saqlanmadi");
    }

    public void getOne(Integer id, HttpServletResponse response) {
        try {
            Optional<Attachment> optionalAttachment = attachmentRepository.findById(id);

            if (optionalAttachment.isPresent()) {

                Attachment attachment = optionalAttachment.get();

                response.setHeader("Content-Disposition", "attachment; filename=\"" + attachment.getName() + "\"");

                response.setContentType(attachment.getContentType());

                FileInputStream fileInputStream = new FileInputStream(uploadDirectorys + "/" + attachment.getName());

                FileCopyUtils.copy(fileInputStream, response.getOutputStream());

            }


        } catch (Exception e) {
            e.printStackTrace();

        }

    }

    public HttpEntity<AttachmentDto> getOneInfo(Integer id) {
        Optional<Attachment> optionalAttachment = attachmentRepository.findById(id);
        if (optionalAttachment.isEmpty()) return ResponseEntity.status(500).body(new AttachmentDto());
        AttachmentDto attachmentDto = attachmentMapper.attachmentToDto(optionalAttachment.get());
        attachmentDto.setFileOriginalName(optionalAttachment.get().getFileOriginalName());

        return ResponseEntity.status(200).body(attachmentDto);
    }

    public HttpEntity<?> delete(Integer id) {

        // BAZADAN ID BOYICHA ATTACHMENTNI TOPIB OLIB KELADI
        Optional<Attachment> attachmentById = attachmentRepository.findAttachmentById(id);
        if (attachmentById.isEmpty())
            return ResponseEntity.ok(new StateMessage("bazada bu id li fayl mavjudmas", false));

        Attachment attachment = attachmentById.get();
            attachmentRepository.delete(attachment);

        // FILE NI SYSTEMADAN VA DBDAN O'CHIRIB TASHLAYDI
        try {
            Files.delete(Path.of(attachment.getPath()));
        } catch (IOException e) {
            return ResponseEntity.ok(new StateMessage(e.getMessage(),false));
        }
        return ResponseEntity.ok(new StateMessage("fayl o'chirildi", true));
    }

    @Transactional
    public HttpEntity<?> editPhotoIsMain(Integer imageId, Long productId) {
        if (editIsMain(imageId,productId))
        return ResponseEntity.ok(new StateMessage("Bu rasm asosiy qilib saqlandi", true));

        return ResponseEntity.ok(new StateMessage("productning bunday rasmi mavjudmas", false));
    }
    //RASM QO'SHAYOTGANDA AGAR RASM ASOSIY RASM BO'LSA BAZADAGI ASOSIY RASMNI O'RNIGA ASOSIY RASM QILIB QO'YADI
    private boolean editIsMain(Integer imageId, Long productId){

        // BAZADAN ID BOYICHA ATTACHMENTNI TOPIB OLIB KELADI
        Optional<Attachment> attachmentById = attachmentRepository.findAttachmentByIdAndProductId(imageId, productId);
        if (attachmentById.isEmpty())
            return false;

        Attachment attachment = attachmentById.get();

        List<Attachment> allMainPhotos = attachmentRepository.findAllByIsprimeIsTrueAndProductId(productId);
        if (!allMainPhotos.isEmpty()) {
            for (Attachment allMainPhoto : allMainPhotos) {
                allMainPhoto.setIsprime(false);
//                attachmentRepository.save(allMainPhoto);
            }
            attachmentRepository.saveAll(allMainPhotos);
        }
        attachment.setIsprime(true);
        attachmentRepository.save(attachment);
        return true;
    }
}
package com.example.specialTechnique.service;

import com.example.specialTechnique.entity.Category;
import com.example.specialTechnique.repository.CategoryRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;

@Service
@RequiredArgsConstructor
public class BaseService {

    private final CategoryRepository categoryRepository;

    public int indexOrderForCategory(int index) {
        List<Category> categoryList = categoryRepository.findAllOrderByIndex();
        int size = categoryList.size();
        if (categoryList.isEmpty()) return 1;
        else if (index >= size) return size + 1;
        return -1;
    }
}

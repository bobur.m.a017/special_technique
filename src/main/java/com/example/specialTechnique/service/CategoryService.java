package com.example.specialTechnique.service;

import com.example.specialTechnique.entity.Category;
import com.example.specialTechnique.mapper.CategoryMapper;
import com.example.specialTechnique.message.StateMessage;
import com.example.specialTechnique.payload.CategoryByIndexDto;
import com.example.specialTechnique.payload.CategoryDto;
import com.example.specialTechnique.payload.CategoryListForIndexDto;
import com.example.specialTechnique.payload.CategoryResDto;
import com.example.specialTechnique.repository.CategoryRepository;
import com.example.specialTechnique.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class CategoryService {
    private final CategoryRepository categoryRepository;
    private final ProductRepository productRepository;
    private final CategoryMapper categoryMapper;

    public HttpEntity<?> add(CategoryDto categoryDto) {

        Optional<Category> optionalCategory
                = categoryRepository.findByName(categoryDto.getName());

        if (optionalCategory.isPresent()) return ResponseEntity.status(500).body("Bu nomli Categoriya Bazada mavjud");

        Category category = categoryRepository.save(new Category(categoryDto.getName(), indexOrderForCategory()));
        return ResponseEntity.ok(new StateMessage("categoriya qo'shildi",true));
    }


    public HttpEntity<?> delete(Integer id) {
        Optional<Category> optionalCategory = categoryRepository.findById(id);

        if (optionalCategory.isEmpty())
            return ResponseEntity.ok(new StateMessage("Bunday Categoriya mavjudmas", false));

        if (productRepository.existsByCategoryId(id))
            return ResponseEntity.ok(new StateMessage("Categoriya ichida maxsulotlar mavjud, Categoriyani o'chira olmaysiz", false));

        categoryRepository.delete(optionalCategory.get());
        return ResponseEntity.ok(new StateMessage(optionalCategory.get().getName() + " nomli Categoriya o'chirildi", true));
    }

    public HttpEntity<?> edit(CategoryDto categoryDto, Integer id) {

        Optional<Category> optionalCategory = categoryRepository.findById(id);

        if (optionalCategory.isEmpty())
            return ResponseEntity.ok(new StateMessage("Bunday Categoriya mavjudmas", false));

        Category category = optionalCategory.get();

        category.setName(categoryDto.getName());

        categoryRepository.save(category);

        return ResponseEntity.ok(new StateMessage("Categoriya nomi " + optionalCategory.get().getName() + "ga o'zgartirildi", true));

    }

    public HttpEntity<?> editIndex(List<CategoryByIndexDto> categorysDto) {

        Set<Integer> set = categorysDto.stream().map(CategoryByIndexDto::getIndex).collect(Collectors.toSet());

        if (set.size() < categorysDto.size())
            return ResponseEntity.ok(new StateMessage("iltimos index takrorlanmas bo'lsin, sizda o'xshash indexlar mavjud", false));
        List<Category> categories = new ArrayList<>();
        for (CategoryByIndexDto categoryDto : categorysDto) {
            Optional<Category> optionalCategory = categoryRepository.findByName(categoryDto.getName());
            if (optionalCategory.isEmpty())
                return ResponseEntity.ok(new StateMessage("Bunday Categoriya mavjudmas", false));

            Category category = optionalCategory.get();
            category.setIndex(categoryDto.getIndex());
            category.setId(categoryDto.getId());
            categories.add(category);
        }
        categoryRepository.saveAll(categories);
        return ResponseEntity.ok(categories);
    }

    public HttpEntity<?> getOne(Integer id) {

        Optional<Category> optionalCategory = categoryRepository.findById(id);
        if (optionalCategory.isEmpty())
            return ResponseEntity.ok(new StateMessage("Bu IDlicategoriya mavjudmas", false));

        Category category = optionalCategory.get();

        return ResponseEntity.status(200).body(category);
    }

    public HttpEntity<?> getAll() {
        List<Category> categoryList = categoryRepository.findAllOrderByIndex();

        //katego'riya bo'lmasa message qaytarmaslik kerak ekan. o'shanga commentda bu
//        if (categoryList.isEmpty()) return ResponseEntity.ok(new StateMessage("Sizda Categoriyalar mavjudmas", false));

        return ResponseEntity.status(200).body(categoryList);
    }


    public int indexOrderForCategory() {
        List<Category> categoryList = categoryRepository.findAllOrderByIndex();
        if (categoryList.isEmpty()) return 1;
        else return categoryList.size() + 1;
    }

}


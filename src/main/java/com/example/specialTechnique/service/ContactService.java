package com.example.specialTechnique.service;

import com.example.specialTechnique.contact.ContactDTO;
import com.example.specialTechnique.entity.ContactEntity;
import com.example.specialTechnique.contact.ContactEntityDTO;
import com.example.specialTechnique.message.StateMessage;
import com.example.specialTechnique.repository.ContactRepository;
import com.example.specialTechnique.response.AddContactResponse;
import com.example.specialTechnique.response.DeleteContactResponse;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Component
public class ContactService {

    private final ContactRepository contactRepository;
    private final SmsService smsService;

    public ContactService(ContactRepository contactRepository, SmsService smsService) {
        this.contactRepository = contactRepository;
        this.smsService = smsService;
    }

    public StateMessage add(ContactDTO dto) throws IOException {
        String token = smsService.getToken();
        if (token != null) {
            if (checkNumber(dto.getMobile_phone())) {
                AddContactResponse addContactResponse = smsService.addContact(dto, token);
                if (addContactResponse != null) {
                    if (addContactResponse.getStatus().equals("success")) {
                        ContactEntityDTO oneContactEntity = smsService.getOneContact(addContactResponse.getData().getContact_id(), token);
                        if (oneContactEntity != null) {
                            contactRepository.save(new ContactEntity(oneContactEntity.getId(), oneContactEntity.getUser_id(), oneContactEntity.getGroup(), oneContactEntity.getCreated_at(), oneContactEntity.getUpdated_at(), oneContactEntity.getName(), oneContactEntity.getEmail(), oneContactEntity.getMobile_phone()));
                            return new StateMessage("Contact muvaffaqiyatli qo`shildi", true);
                        }
                    }
                }
            }
        }
        return new StateMessage("Xatolik qo`shilmadi", false);
    }

    public StateMessage delete(Integer id) throws IOException {

        String token = smsService.getToken();
        if (token != null) {
            DeleteContactResponse deleteContactResponse = smsService.deleteContact(id, token);

            ContactEntity contactEntity = contactRepository.findByIdContact(id).get();
            contactRepository.delete(contactEntity);
            return new StateMessage("Contact muvaffaqiyatli o`chirildi", true);
        }
        return new StateMessage("Xatolik o`chirilmadi", false);
    }

    public ContactEntity get(Integer id) {
        return contactRepository.findByIdContact(id).get();
    }

    public List<ContactEntity> getAll() {
        return contactRepository.findAll();
    }

    public StateMessage addAll(MultipartFile multipartFile) throws IOException, InvalidFormatException {

        List<ContactDTO> list = new ArrayList<>();
        InputStream inputStream = multipartFile.getInputStream();

        OPCPackage fis = OPCPackage.open(inputStream);

        XSSFWorkbook wb = new XSSFWorkbook(fis);
        XSSFSheet sheet = wb.getSheetAt(0);
        for (Row row : sheet) {
            ContactDTO dto = new ContactDTO();
            int count = 1;
            for (Cell cell : row) {

                if (!cell.getStringCellValue().equals("Ism*") && !cell.getStringCellValue().equals("Telefon*")) {

                    if (count == 1)
                        dto.setName(cell.getStringCellValue());
                    if (count == 2)
                        dto.setMobile_phone(cell.getStringCellValue());
                }

                count++;
            }
            list.add(dto);
        }


        int count = 0;
        int count1 = 0;
        for (ContactDTO dto : list) {
            if (dto.getMobile_phone() != null && dto.getName() != null) {
                if ((!dto.getName().equals("")) && (!dto.getMobile_phone().equals(""))) {
                    dto.setGroupId("client");
                    dto.setEmail("test@test.uz");
                    if (dto.getName() != null && dto.getMobile_phone() != null && dto.getEmail() != null && dto.getGroupId() != null) {
                        StateMessage add = add(dto);
                        if (add.isState()) {
                            count++;
                        }
                    }

                }
            }
        }
        return new StateMessage(count + " ta contact qo`shildi", true);
    }

    public boolean checkNumber(String number) {
        Optional<ContactEntity> optional = contactRepository.findByPhoneNumber(number);

        return optional.isEmpty();
    }
}

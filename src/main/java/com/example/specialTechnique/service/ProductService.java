package com.example.specialTechnique.service;

import com.example.specialTechnique.entity.Attachment;
import com.example.specialTechnique.entity.Category;
import com.example.specialTechnique.entity.FeaturesEntity;
import com.example.specialTechnique.entity.Product;
import com.example.specialTechnique.mapper.ProductMapper;
import com.example.specialTechnique.message.StateMessage;
import com.example.specialTechnique.payload.*;
import com.example.specialTechnique.repository.AttachmentRepository;
import com.example.specialTechnique.repository.CategoryRepository;
import com.example.specialTechnique.repository.FeaturesEntityRepository;
import com.example.specialTechnique.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ProductService {
    private final CategoryRepository categoryRepository;
    private final ProductRepository productRepository;
    private final ProductMapper productMapper;
    private final AttachmentRepository attachmentRepository;
    private final FeaturesEntityRepository featuresEntityRepository;

    @Value(value = "${limit}")
    private int limit;

    public HttpEntity<?> add(ProductDto productDto) {

        //PRODUCT QO'SHMOQCHI BO'LGAN CATEGORIYA MAVJUD BO'MASA
        Optional<Category> optionalCategory = categoryRepository.findById(productDto.getCategoryId());

        if (optionalCategory.isEmpty())
            return ResponseEntity.ok(new StateMessage("bu categoriya mavjudmas", false));

        //DTO DA KELGAN MALUMOTLAR ORQALI YANGI PRUDUCT HOSIL QILIBERADI
        Product product = productMapper.dtoToProduct(productDto);

        Product saveProduct = productRepository.save(product);

        featuresEntityRepository.saveAll(getFeatures(productDto.getFeaturesDtoList(), saveProduct));

        return ResponseEntity.ok(new StateMessage("maxsulot qo'shildi id = " + saveProduct.getId(), true));
    }

    public HttpEntity<?> getOne(Long id) {

        Optional<Product> optionalProduct = productRepository.findById(id);
        if (optionalProduct.isEmpty())
            return ResponseEntity.ok(new StateMessage("bu product mavjudmas", false));
        Product product = optionalProduct.get();


        //PRODUCTNI BIZGA KERAK FORMADAGI DTOGA AYLANTIRIBERADI
        GetOneProductDto getOneProductDto = productToDto(product);

        return ResponseEntity.ok(getOneProductDto);
    }

    public HttpEntity<?> getView() {

        List<ViewDto> categoryDtoList = new ArrayList<>();

        List<Category> categoryList = categoryRepository.findAllOrderByIndex();

        for (Category category : categoryList) {

            ViewDto viewDto = new ViewDto();
            viewDto.setCategoryId(category.getId());
            viewDto.setCategoryName(category.getName());
            viewDto.setCategoryIndex(category.getIndex());


            //CATEGORIYAGA TEGISHLI PRODUCTLARNI OLYAPMIZ
            List<Product> products = productRepository.findByCategoryIdByLimit(category.getId(), limit);

            List<ProductForCategoryViewDto> productForCategoryViewDtos = new ArrayList<>();
            for (Product product : products) {

                //BITTA PRODUCTGA BITTA ASOSIY RASMNI BIRIKTIRIB TOLIQ QAYTARADIGAN METHOD
                ProductForCategoryViewDto oneProductDto = getOneProductDto(product);
                productForCategoryViewDtos.add(oneProductDto);
            }

            viewDto.setLimitProducts(productForCategoryViewDtos);

            categoryDtoList.add(viewDto);
        }

        return ResponseEntity.ok(categoryDtoList);

    }


    public HttpEntity<?> edit(Long id, ProductDto productDto) {

        if (!categoryRepository.existsById(productDto.getCategoryId())) {
            return ResponseEntity.ok(new StateMessage("o'zgartirmoqchi bo'lgan productni mavjud bo'lmagan categoriyaga qo'shmoqchisiz", false));
        }
        Optional<Product> optionalProduct = productRepository.findById(id);
        if (optionalProduct.isEmpty())
            return ResponseEntity.ok(new StateMessage("o'zgartirmoqchi bo'lgan product mavjudmas", false));

        Product product = optionalProduct.get();

        //ESKISI O'CHIRILADI
        List<FeaturesEntity> featuresDtoList = new ArrayList<>(product.getFeaturesDtoList());

        //DTODAGI MA'LUMOTLARNI PRODUCTGA SET QILIBERADI
        productMapper.updateProductToDto(productDto, product);

        //YANGI HUSUSIYATLARNI BAZAGA SAQLAYDI
        getFeatures(productDto.getFeaturesDtoList(), product);

        productRepository.save(product);

        featuresEntityRepository.deleteAll(featuresDtoList);

        return ResponseEntity.ok(new StateMessage("Product muvaffaqiyatli o'zgartirildi", true));
    }


    public HttpEntity<?> delete(Long id) {

        //BAZADAN PRODUCTNI OLYAPDI AKS HOLDA ERROR
        Optional<Product> optionalProduct = productRepository.findById(id);
        if (optionalProduct.isEmpty()) return ResponseEntity
                .ok(new StateMessage("o'chirmoqchi bo'lgan malumotiz bazada mavjudmas", false));
        Product product = optionalProduct.get();

        //PRODUCTNING HUSUSIYATLARINI O'CHIRADI
        featuresEntityRepository.deleteAll(product.getFeaturesDtoList());

        //PRODUCTNING RASMLARINI SYSTEMADAN VA BAZADAN TOZALAB TASHAYDI;
        deleteAttachments(product.getId());

        productRepository.delete(product);

        return ResponseEntity.ok(new StateMessage("fayl o'chirildi", true));
    }


//--------------------------Qoshimcha yordamchi methodlar -------------------------

    //PRODUCTNING HUSUSIYATLARINI BAZAGA SAQLABERADI
    public List<FeaturesEntity> getFeatures(List<FeaturesDto> list, Product product) {
        List<FeaturesEntity> arrayList = new ArrayList<>();
        for (FeaturesDto featuresDto : list) {
            arrayList.add(new FeaturesEntity(featuresDto.getName(), featuresDto.getValue(), product.getId()));
        }
        return featuresEntityRepository.saveAll(arrayList);
    }

    //PRODUCTNI BIZGA KERAK FORMADAGI DTOGA AYLANTIRIBERADI
    private GetOneProductDto productToDto(Product product) {

        GetOneProductDto oneProductDto = productMapper.productToDto(product);

        //AGAR PRODUCTNING QO'SHIMCHA HUSUSIYATLARIMAVJUD BO'LSA UNI BAZADAN OLIB DTOGA O'RAYDI
        List<FeaturesEntity> featuresDtoList = product.getFeaturesDtoList();
        if (!featuresDtoList.isEmpty()) {
            List<FeaturesDto> featuresDtos = dbToFeaturesDto(featuresDtoList);
            oneProductDto.setFeaturesDtoList(featuresDtos);
        }

        List<Attachment> attachmentList = attachmentRepository.findAllByProductIdOrderByIsprimeDesc(product.getId());
        List<AttachmentDto> attachmentDtos = new ArrayList<>();
        for (Attachment attachment : attachmentList) {
            attachmentDtos.add(new AttachmentDto(attachment.getId(),attachment.getName(),attachment.getFileOriginalName(),attachment.getSize(),attachment.getContentType()));
        }
        oneProductDto.setListBytes(attachmentDtos);

        return oneProductDto;
    }

    //ATTACHMENTNI BYTGA O'GIRIBERADI
    private byte[] attachmentToBytes(Attachment attachment) {
        try {
            File file = new File(attachment.getPath());
            Path path = Paths.get(file.getPath());
            return Files.readAllBytes(path);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    //ATTACHMENLAR LISTINI BYTE[] LISTGA O'GIRIB QAYTARIBERADI
    private List<AttachmentByteIdDto> attachmentsToBytes(List<Attachment> attachmentList) {
        ArrayList<AttachmentByteIdDto> list = new ArrayList<>();
        for (Attachment attachment : attachmentList) {
            list.add(new AttachmentByteIdDto(attachment.getId(),
                    attachmentToBytes(attachment)));
        }
        return list;
    }

    //PRODUCT O'CHIRILGANDA UNGA TEGISHLI RASMLARNI O'CHIRIB KELADI
    public boolean deleteAttachments(Long productId) {
        try {
            // BAZADAN ID BOYICHA ATTACHMENTNI TOPIB OLIB KELADI
            List<Attachment> allByProductId = attachmentRepository.findAllByProductIdOrderByIsprimeDesc(productId);

            for (Attachment attachment : allByProductId) {
                Files.delete(Path.of(attachment.getPath()));
            }
            attachmentRepository.deleteAll(allByProductId);
        } catch (IOException e) {
            return false;
        }
        return true;
    }

    //BITTA PRODUCTGA BITTA ASOSIY RASMNI BIRIKTIRIB QAYTARADIGAN METHOD
    public ProductForCategoryViewDto getOneProductDto(Product product) {

        ProductForCategoryViewDto productForCategoryViewDto = productMapper.productToViewDto(product);

        //KERAK BO'LSA COMMITDAN CHIQARILSA HUSUSIYATLARNI OLIB BERADI
//        productForCategoryViewDto.setFeaturesDtoList(product.getFeaturesDtoList());

        productForCategoryViewDto.setId(product.getId());

        //KIRIB KELGAN PRODUCTNING ASOSIY RASMINI OLIBERADI AKS HOLDA BIRINCHI UCHRAGAN RASMINI OLIBERADI
        Optional<Attachment> firstByProductIdAndPrimeIsTrue = attachmentRepository.findFirstByProductIdAndIsprimeIsTrue(product.getId());
        Attachment attachment;

        if (firstByProductIdAndPrimeIsTrue.isEmpty()) {
            attachment = attachmentRepository.findFirstByProductId(product.getId()).orElse(null);
        } else
            attachment = firstByProductIdAndPrimeIsTrue.get();

//        if (attachment != null) {
//
//            //ATTACHMENTNI BYTGA O'GIRIBERADI
//            byte[] bytes = attachmentToBytes(attachment);
//
//            assert bytes != null;
//            productForCategoryViewDto.setListBytes(bytes);
//        }
        return productForCategoryViewDto;
    }

    //DTO DA KELGAN PRODUCT HUSUSIYATLARINI BAZAGA SAQLABERADI
    public void setFeaturesToDb(Product product, List<FeaturesDto> featuresDtoList) {

        if (!featuresDtoList.isEmpty()) {
            List<FeaturesEntity> entityList = featuresDtoList.stream().map(FeaturesEntity::new).collect(Collectors.toList());
            product.setFeaturesDtoList(entityList);
            featuresEntityRepository.saveAll(entityList);
        }
    }

    //DB DAGI PRODUCT HUSUSIYATLARINI DTO LISTGA GA O'RAB QAYTARADI
    public List<FeaturesDto> dbToFeaturesDto(List<FeaturesEntity> featuresEntities) {
        return featuresEntities.stream().map(featuresEntity -> new FeaturesDto(
                featuresEntity.getName(),
                featuresEntity.getValue()
        )).collect(Collectors.toList());
    }

    public HttpEntity<CustomPage<ProductForCategoryViewDto>> pageProduct(int page, int size, Integer categoryId) {
        Pageable pageable = PageRequest.of(page, size);
        Page<Product> productPage = productRepository.findAllByCategoryIdOrderByUpdatedAtDesc(categoryId, pageable);
        CustomPage<ProductForCategoryViewDto> getCustomPageDto = pageProductToCustomPageDto(productPage);
        return ResponseEntity.ok(getCustomPageDto);
    }

    private CustomPage<ProductForCategoryViewDto> pageProductToCustomPageDto(Page<Product> pageProduct) {
        return new CustomPage<>(
                pageProduct.getContent().stream().map(this::getOneProductDto).collect(Collectors.toList()),
                pageProduct.getNumberOfElements(),
                pageProduct.getNumber(),
                pageProduct.getTotalElements(),
                pageProduct.getTotalPages(),
                pageProduct.getSize()
        );


    }

    public HttpEntity<?> search(String search) {
        List<GetOneProductDto> list = new ArrayList<>();
        if (search != null){

            for (Product product : productRepository.findAll()) {
                if (product != null) {
                    if (product.getName().length() > search.length()) {
                        if (product.getName().startsWith(search) || check(product, search)) {
                            list.add( productMapper.productToDto(product));
                        }
                    }
                }
            }
        }

        return ResponseEntity.ok(list);
    }

    public boolean check(Product product, String search) {
        if (product.getFeaturesDtoList() != null) {
            for (FeaturesEntity featuresEntity : product.getFeaturesDtoList()) {
                if (featuresEntity != null) {
                    if (featuresEntity.getValue() != null && featuresEntity.getName() != null) {
                        if (featuresEntity.getValue().startsWith(search)) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }
}







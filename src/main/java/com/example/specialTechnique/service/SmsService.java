package com.example.specialTechnique.service;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import com.example.specialTechnique.company.Company;
import com.example.specialTechnique.entity.ContactEntity;
import com.example.specialTechnique.contact.ContactDTO;
import com.example.specialTechnique.contact.ContactEntityDTO;
import com.example.specialTechnique.repository.ContactRepository;
import com.example.specialTechnique.message.StateMessage;
import com.example.specialTechnique.response.AddContactResponse;
import com.example.specialTechnique.response.DeleteContactResponse;
import com.example.specialTechnique.response.SendSmsResponse;
import com.example.specialTechnique.sms.SmsDto;
import com.example.specialTechnique.token.TokenDTO;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import okhttp3.*;
import org.springframework.stereotype.Service;


@Service
public record SmsService(ContactRepository contactRepository) {


    public String getToken() throws IOException {

        okhttp3.OkHttpClient client = new okhttp3.OkHttpClient().newBuilder()
                .build();
        MediaType mediaType = MediaType.parse("text/plain");
        MultipartBody body = new MultipartBody.Builder().setType(MultipartBody.FORM)
                .addFormDataPart("email", "agromashelituz@gmail.com")
                .addFormDataPart("password", "DaRZD8FJSWayVQfamcfUZwxzONbVwWif5hVYus0A")
                .build();
        Request request = new Request.Builder()
                .url("http://notify.eskiz.uz/api/auth/login")
                .method("POST", body)
                .build();
        Response response = client.newCall(request).execute();
//        System.out.println(response);
        Gson gson = new Gson();
        TokenDTO entity = gson.fromJson(Objects.requireNonNull(response.body()).string(), TokenDTO.class);

//        System.out.println(entity.toString());
//        System.out.println(entity.getData().toString());
        return entity.getData().getToken();
    }

    public String refreshToken(String token) throws IOException {

        OkHttpClient client = new OkHttpClient().newBuilder()
                .build();
        MediaType mediaType = MediaType.parse("text/plain");
        RequestBody body = RequestBody.create(mediaType, "");
        Request request = new Request.Builder()
                .url("http://notify.eskiz.uz/api/auth/refresh")
                .method("PATCH", body)
                .build();
        Response response = client.newCall(request).execute();
        Gson gson = new Gson();
        TokenDTO entity = gson.fromJson(Objects.requireNonNull(response.body()).string(), TokenDTO.class);

        return entity.getData().getToken();
    }

    public Company getUser(String token) throws IOException {
        OkHttpClient client = new OkHttpClient().newBuilder()
                .build();
        MediaType mediaType = MediaType.parse("text/plain");
        RequestBody body = RequestBody.create(mediaType, "");
        Request request = new Request.Builder()
                .url("http://notify.eskiz.uz/api/auth/user")
                .method("GET", null)
                .addHeader("Authorization", "Bearer " + token)
                .build();
        Response response = client.newCall(request).execute();

        Type type = new TypeToken<Company>() {
        }.getType();
        Gson gson = new Gson();
        Company entity = gson.fromJson(Objects.requireNonNull(response.body()).string(), type);
        return entity;

    }

    public AddContactResponse addContact(ContactDTO contact, String token) throws IOException {
        try {
            OkHttpClient client = new OkHttpClient().newBuilder()
                    .build();
            MediaType mediaType = MediaType.parse("text/plain");
            RequestBody body = new MultipartBody.Builder().setType(MultipartBody.FORM)
                    .addFormDataPart("name", contact.getName())
                    .addFormDataPart("email", contact.getEmail())
                    .addFormDataPart("group", contact.getGroupId())
                    .addFormDataPart("mobile_phone", contact.getMobile_phone())
                    .build();
            Request request = new Request.Builder()
                    .url("http://notify.eskiz.uz/api/contact")
                    .method("POST", body)
                    .addHeader("Authorization", "Bearer " + token)
                    .build();
            Response response = client.newCall(request).execute();

            Type type = new TypeToken<AddContactResponse>() {
            }.getType();
            Gson gson = new Gson();
            return gson.fromJson(Objects.requireNonNull(response.body()).string(), type);
        } catch (Exception e) {
            return null;
        }
    }

    public DeleteContactResponse deleteContact(Integer id, String token) throws IOException {
        OkHttpClient client = new OkHttpClient().newBuilder()
                .build();
        MediaType mediaType = MediaType.parse("text/plain");
        RequestBody body = RequestBody.create(mediaType, "");
        Request request = new Request.Builder()
                .url("http://notify.eskiz.uz/api/contact/" + id)
                .method("DELETE", body)
                .addHeader("Authorization", "Bearer " + token)
                .build();
        Response response = client.newCall(request).execute();
        Gson gson = new Gson();
        return gson.fromJson(Objects.requireNonNull(response.body()).string(), DeleteContactResponse.class);
    }

    public ContactEntityDTO getOneContact(Integer id, String token) throws IOException {
        try {
            OkHttpClient client = new OkHttpClient().newBuilder()
                    .build();
            MediaType mediaType = MediaType.parse("text/plain");
            RequestBody body = RequestBody.create(mediaType, "test");
            Request request = new Request.Builder()
                    .url("http://notify.eskiz.uz/api/contact/" + id)
                    .method("GET", null)
                    .addHeader("Authorization", "Bearer " + token)
                    .build();
            Response response = client.newCall(request).execute();

            GsonBuilder gsonBuilder = new GsonBuilder();

            Gson gson = new Gson();
            Type type = new TypeToken<List<ContactEntityDTO>>() {
            }.getType();
            List<ContactEntityDTO> list = gson.fromJson(Objects.requireNonNull(response.body()).string(), type);
            if (list.size() > 0)
                return list.get(0);
        } catch (Exception e) {
            return null;
        }
        return null;
    }

    public SendSmsResponse sendSmsEskiz(String message, ContactEntity contactEntity, String token) throws IOException {
        try {
            OkHttpClient client = new OkHttpClient().newBuilder()
                    .build();
            MediaType mediaType = MediaType.parse("text/plain");
            RequestBody body = new MultipartBody.Builder().setType(MultipartBody.FORM)
                    .addFormDataPart("mobile_phone", contactEntity.getPhoneNumber())
                    .addFormDataPart("message", message)
                    .addFormDataPart("from", "4546")
                    .addFormDataPart("callback_url", "http://governess.uz")
                    .build();
            Request request = new Request.Builder()
                    .url("http://notify.eskiz.uz/api/message/sms/send")
                    .method("POST", body)
                    .addHeader("Authorization", "Bearer " + token)
                    .build();
            Response response = client.newCall(request).execute();

            Gson gson = new Gson();
            return gson.fromJson(Objects.requireNonNull(response.body()).string(), SendSmsResponse.class);
        } catch (Exception e) {
            return null;
        }
    }

    public StateMessage sendSms(SmsDto dto) throws IOException {

        StringBuilder str = new StringBuilder();
        String token = getToken();
        int count = 0;
        int length = dto.getContactIdList().size();
        if (token != null) {
            for (Integer contactId : dto.getContactIdList()) {
                Optional<ContactEntity> byId = contactRepository.findById(contactId);
                if (byId.isPresent()) {
                    ContactEntity contactEntity = byId.get();
                    SendSmsResponse sendSmsResponse = sendSmsEskiz(dto.getMessage(), contactEntity, getToken());
                    if (sendSmsResponse != null) {
                        if (sendSmsResponse.getStatus().equals("waiting")) {
                            count++;
                        }
                    }
                }
            }
            return new StateMessage("Jami " + length +"ta contactdan.   "+count+"ta contactga yuborildi.", true);
        }
        return new StateMessage("Xatolik", false);
    }

    public Company getBalance() throws IOException {
        String token = getToken();
        if (token != null) {
            return getUser(token);
        }
        return null;
    }
}

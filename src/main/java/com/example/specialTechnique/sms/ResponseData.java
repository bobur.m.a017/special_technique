package com.example.specialTechnique.sms;

public class ResponseData {
    private String token;

    @Override
    public String toString() {
        return "Data{" +
                "token='" + token + '\'' +
                '}';
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}

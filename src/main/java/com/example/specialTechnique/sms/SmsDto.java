package com.example.specialTechnique.sms;

import java.util.List;

public class SmsDto {
    private List<Integer> contactIdList;
    private String message;

    public List<Integer> getContactIdList() {
        return contactIdList;
    }

    public void setContactIdList(List<Integer> contactIdList) {
        this.contactIdList = contactIdList;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}

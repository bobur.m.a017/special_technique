package com.example.specialTechnique.user;

public class ResponseUsers {

    private Integer id;
    private String role;
    private String token;
    private Boolean success;
    private String username;
    private String fullName;

    public ResponseUsers(String username) {
        this.username = username;
    }

    public ResponseUsers(Integer id, String role, String token, Boolean success, String text) {
        this.id = id;
        this.role = role;
        this.token = token;
        this.username = text;
        this.success = success;
    }


    public ResponseUsers() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }


}

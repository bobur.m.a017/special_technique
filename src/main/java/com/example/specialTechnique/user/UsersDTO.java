package com.example.specialTechnique.user;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class UsersDTO {

    @NotNull
    @Size(min = 5, max = 20)
    private String userName;

    @NotNull
    @Size(min = 8, max = 25)
    private String password;

    @NotNull
    @Size(min = 5)
    private String fullName;

    @NotNull
    @Size(min = 13, max = 13)
    private String phoneNumber;

    @NotNull
    private Integer roleId;

    @NotNull
    private Integer districtId;

    @NotNull
    private Integer neighborhoodId;

    @NotNull
    @Size(min = 5, max = 50)
    private String street;
}

package com.example.specialTechnique.user;

import com.example.specialTechnique.config.ApplicationUsernamePasswordAuthenticationFilter;
import com.example.specialTechnique.entity.User;
import com.example.specialTechnique.repository.UserRepository;
import org.springframework.stereotype.Service;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Objects;

@Service
public final class UsersService {
    private final UserRepository userRepository;
    private final ApplicationUsernamePasswordAuthenticationFilter authenticationFilter;

    UsersService(
            UserRepository userRepository,
            ApplicationUsernamePasswordAuthenticationFilter authenticationFilter
    ) {
        this.userRepository = userRepository;
        this.authenticationFilter = authenticationFilter;
    }

    public UserRepository usersRepository() {
        return userRepository;
    }

    public ApplicationUsernamePasswordAuthenticationFilter authenticationFilter() {
        return authenticationFilter;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (obj == null || obj.getClass() != this.getClass()) return false;
        var that = (UsersService) obj;
        return Objects.equals(this.userRepository, that.userRepository) &&
                Objects.equals(this.authenticationFilter, that.authenticationFilter);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userRepository, authenticationFilter);
    }

    @Override
    public String toString() {
        return "UsersService[" +
                "usersRepository=" + userRepository + ", " +
                "authenticationFilter=" + authenticationFilter + ']';
    }


    public ResponseUsers signIn(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        ResponseUsers responseUser = authenticationFilter.successfulAuthentication(request, response);
        if (responseUser.getSuccess()) {
            User user = userRepository.findByUserName(responseUser.getUsername()).get();
            responseUser.setFullName(user.getUsername() != null ? user.getUsername() : null);
        }
        return responseUser;
    }
}
